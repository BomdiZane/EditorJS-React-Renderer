import { MARGIN } from '../../style/template';

const rawOutputStyle = { 
	width: '100%',
	margin: MARGIN
};

export default rawOutputStyle;
